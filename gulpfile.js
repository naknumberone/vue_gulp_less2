var gulp = require("gulp");
var less = require('gulp-less');
var path = require('path');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var order = require("gulp-order");

gulp.task('less', function () {
  return gulp.src('./src/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('html', function () {
  return gulp.src('./src/**/*.html')
    .pipe(gulp.dest('./dist'));
});

gulp.task('php', function () {
  return gulp.src('./src/php/**/*.php')
    .pipe(gulp.dest('./dist'));
});

gulp.task('js', function () {
  gulp
    .src("src/js/**/*.js")
    .pipe(order([
    "areaFilling.js",
    "app.js"]))
    .pipe(concat('app-prod.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./dist'))
});

gulp.task("default", ["less", "js", "html", "php"]);
