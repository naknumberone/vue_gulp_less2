<?php
  header("Content-type:application/json");

  $json = array();

  switch($_GET['set']){

    case '1':
    $json = array(
      'width' => "5",
      'height' => "5",
      'step' => "100",
      'bgColor' => "white",
      'marginRight' => '400'
    ); break;

    case '2':
    $json = array(
      'width' => "3",
      'height' => "3",
      'step' => "100",
      'bgColor' => "white",
      'marginRight' => '400'
    ); break;

    case '3':
    $json = array(
      'width' => "3",
      'height' => "2",
      'step' => "100",
      'bgColor' => "white",
      'marginRight' => '400'
    ); break;
  }

  $jsonstring = json_encode($json);
  echo $jsonstring;
